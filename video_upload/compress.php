<?php
	//CLI System Call

	//ffmpeg -i aaaa.webm -s 480x320 -c:a copy akash_output.mp4

	include_once('config.php');
	
	$query 	= 	'SELECT * FROM videos WHERE status='. 0;
	
	$result = 	$con->query($query);

	if ($result->num_rows > 0) 
	{
	    // output data of each row
	    $dir 				= 	__DIR__ . '/uploads/';
	    $ffmpeg 			=	'ffmpeg -i ';
	    $resize				=	'320x240';//480x320
	    $resizeCmd			=	' -s ' . $resize;
	    $copyCmd 			=	' -c:a copy ';
	    $compressedFileDir  =	__DIR__. '/compressed/';

	    $deleteCmd 			=	'rm ';
	    $changePermissionCmd= 'chmod 777 -R ';
	
	    while($row 	= 	$result->fetch_assoc()) 
	    {
	    	$video_path 		=	$row['video_path'];
	        $file 				=	$dir . $video_path;

	        //changing the file permission so that it can be deleted after compression
	        $systemChangePermissionCmd = $changePermissionCmd . $file;
	        system($systemChangePermissionCmd);

	        $compressedFilePath =	$compressedFileDir . $video_path;	
	        $systemCall			=	$ffmpeg . $file . $resizeCmd . $copyCmd . $compressedFilePath; 
	        //echo "<br><hr>".$systemCall."<br>";
	        system($systemCall);

	        $deleteSystemCall 	=	$deleteCmd . $file;
	        system($deleteSystemCall);

	        $query = 'UPDATE videos SET status = '. 1 .' WHERE video_path = "' . $video_path .'"';
		
			if ($con->query($query) != TRUE) 
			{
			    echo "Error: " . $query . "<br>" . $con->error;
			}

	    }
	}
	$con->close();
?>
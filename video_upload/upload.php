<?php
	include_once('config.php');
	$chunk_num 		= 	$_POST['chunk_num'];
	$video_key 		=	$_POST['video_key'];
	$video_file 	= 	$_FILES['video_file'];
	
	$target_dir 	= 	"uploads/";
	$filename 		=	base64_encode($video_key);
	$blob_name 		=	basename($video_file["name"]) . '_' . $chunk_num . '_' . $filename .'.mp4';
	$target_file 	= 	$target_dir . $blob_name;
	
	if (move_uploaded_file($video_file["tmp_name"], $target_file)) 
	{
		$query = 'INSERT INTO chunks(video_key, chunk_path, chunk_num) value("'.$video_key.'", "'.$blob_name.'", "'.$chunk_num.'")';
		
		if ($con->query($query) === TRUE) 
		{
		    echo "New record created successfully.";
		} 
		else 
		{
		    echo "Error: " . $query . "<br>" . $con->error;
		}

		$con->close();

    	echo "<br>The file " . $blob_name ." has been uploaded.";
    } 
    else 
    {
        echo "Sorry, there was an error uploading your file.";
    }
		
?>
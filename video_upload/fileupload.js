onmessage = function(e)
{
	file_chunk 		= 	e.data['blob'];
	chunk_num 		= 	e.data['chunk_num'];
	file_name 		=	e.data['video_key']
	
	var formData 	= 	new FormData();
	formData.append('chunk_num', chunk_num);
	formData.append('video_file', file_chunk);
	formData.append('video_key', file_name);

	var url 	= 	'upload.php?chunk_num=' + chunk_num; 
	var xhr 	= 	new XMLHttpRequest();
	xhr.open('POST',url,true);
	xhr.onreadystatechange = function(e)
	{
		//if(e.target.readyState == 4 && e.target.status == 200)
		//{
			postMessage({
				'readyState':e.target.readyState,
				'status':e.target.status,
				'chunk_num':chunk_num
			});
		//}
	}

	xhr.send(formData);
}